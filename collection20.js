
Database = require('arangojs').Database;

aqlQuery = require('arangojs').aqlQuery;


db = new Database();

db.useBasicAuth("root");

db.useDatabase('nodeArangoWebAppDB');

collection = db.collection('User');

doc = {
  username: 'firstDocument',
  email:    'ashish@ashish.com'
  
};



collection.save(doc).then(
  meta => console.log('Document saved:', meta._rev),
  err => console.error('Failed to save document:', err)
);

collection.all().then(
  cursor => cursor.map(doc => doc._key)
).then(
  keys => console.log('All keys:', keys.join(', ')),
  err => console.error('Failed to fetch all documents:', err)
);

